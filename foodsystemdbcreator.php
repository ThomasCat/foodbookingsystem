<!--
busreservationsystem
    Copyright (C) 2020  Owais Shaikh

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<html>
    <body>
        <h2>Database controller for Food Delivery System</h2>
        <hr>
        <form action="foodsystemdbcreator.php" method="post">
            <input type="submit" name="create" value="CREATE DATABASE"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="submit" name="delete" value="DELETE DATABASE"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="submit" name="populate" value="POPULATE DATABASE"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <b><a href="index.php">Go to Food booking project</a></b>
        </form>
        <h3>Output:</h3>
    </body>
</html>

<?php
if($_SERVER['REQUEST_METHOD'] == "POST" and isset($_POST['create'])){
    createdb();
}else if($_SERVER['REQUEST_METHOD'] == "POST" and isset($_POST['delete'])){
    deletedb();
}else if($_SERVER['REQUEST_METHOD'] == "POST" and isset($_POST['populate'])){
    populatedb();
}


function createdb(){
    $server="localhost";
    $username="root";
    $password="root";
    $connection=mysqli_connect($server, $username, $password);

    if(!$connection){
        die("Login error: ".mysqli_connect_error().", check username or password, check if mySQL is running or contact Owais immediately!");
    }

    $dbcreate = "CREATE DATABASE foodsystem;";

    if (mysqli_query($connection, $dbcreate)){
        echo "Database created successfully\n<br>";
    }else{
        echo mysqli_error($connection);
        echo "<br>";
    }

    $dbuse="USE foodsystem;";

    if (mysqli_query($connection, $dbuse)){
        echo "Selected database\n<br>";
    }else{
        echo "Error selecting database: <br>" . mysqli_error($connection);
    }    


    $tablecreatecustomer="CREATE TABLE customer(
        Cust_Name varchar(30),
        Cust_ID int primary key,
        Phone_No bigint(10)
    );";
    $tablecreaterestaurant="CREATE TABLE restaurant(
        FSSAI_ID bigint(14) primary key,
        Name char(50),
        Phone_No bigint(10),
        Type char(20),
        Address varchar(60),
        Manager bigint(12),
        FOREIGN KEY (Manager) REFERENCES staff(Staff_AadharNo)
    );";
    $tablecreatelocation="CREATE TABLE location(
        Cust_ID int primary key,
        City varchar(20),
        Street varchar(20),
        Area varchar(20),
        Plot varchar(5),
        FOREIGN KEY (Cust_ID) REFERENCES customer(Cust_ID)
    );";
    $tablecreatestaff="CREATE TABLE staff(
        Staff_AadharNo bigint(12) primary key,
        Staff_Name varchar(30),
        Staff_Position varchar(20),
        Experience int
    );";
    $tablecreatedeliveryman="CREATE TABLE deliveryman(
        Deliveryman_AadharNo bigint(12) primary key,
        Deliveryman_Name varchar(30),
        Deliveryman_Address varchar(60),
        Deliveryman_Photo blob
    );";
    $tablecreateitem="CREATE TABLE item(
        Item_ID int primary key,
        Item varchar(40),
        Price int NOT NULL,
        Status int
    );";
    $tablecreateorder="CREATE TABLE orders(
        Order_ID int primary key,
        date_time datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
        Order_Amount int NOT NULL
    );";
    $tablecreateitemsordered="CREATE TABLE items_ordered(
        Cust_ID int primary key,
        Order_ID int,
        Item_ID int,
        Quantity int,
        FOREIGN KEY (Cust_ID) REFERENCES customer(Cust_ID),
        FOREIGN KEY (Order_ID) REFERENCES orders(Order_ID),
        FOREIGN KEY (Item_ID) REFERENCES item(Item_ID)
    );";

    $tablecreatevehicle="CREATE TABLE vehicle(
        Type varchar(10),
        Vehicle_Number varchar(10) primary key
    );";
    $tablecreatetransport="CREATE TABLE transport(
        Cust_ID int,
        Deliveryman_AadharNo bigint(12) primary key,
        Distance int,
        Route varchar(60),
        Vehicle_Number varchar(10),
        FOREIGN KEY (Deliveryman_AadharNo) REFERENCES deliveryman(Deliveryman_AadharNo),
        FOREIGN KEY (Vehicle_Number) REFERENCES vehicle(Vehicle_Number)
    );";
    $tablecreatepayment="CREATE TABLE payment(
        Txn_ID int primary key,
        Cust_ID int,
        Amount int,
        GST float,
        Mode varchar(10),
        Cardholder varchar(30),
        Card_No bigint(16),
        Expiry varchar(5),
        CVV int,
        FOREIGN KEY (Cust_ID) REFERENCES customer(Cust_ID)
    );";

    if (mysqli_query($connection, $tablecreatecustomer)){
        echo "Created customer table\n<br>";
    }else{
        echo mysqli_error($connection);
        echo "<br>";
    }    

    if (mysqli_query($connection, $tablecreatelocation)){
        echo "Created location table\n<br>";
    }else{
        echo mysqli_error($connection);
        echo "<br>";
    }

    if (mysqli_query($connection, $tablecreatestaff)){
        echo "Created staff table\n<br>";
    }else{
        echo mysqli_error($connection);
        echo "<br>";
    }

    if (mysqli_query($connection, $tablecreaterestaurant)){
        echo "Created restaurant table\n<br>";
    }else{
        echo mysqli_error($connection);
        echo "<br>";
    }

    if (mysqli_query($connection, $tablecreatedeliveryman)){
        echo "Created deliveryman table\n<br>";
    }else{
        echo mysqli_error($connection);
        echo "<br>";
    }

    if (mysqli_query($connection, $tablecreateitem)){
        echo "Created item table\n<br>";
    }else{
        echo mysqli_error($connection);
        echo "<br>";
    }

    if (mysqli_query($connection, $tablecreateorder)){
        echo "Created order table\n<br>";
    }else{
        echo mysqli_error($connection);
        echo "<br>";
    }

    if (mysqli_query($connection, $tablecreateitemsordered)){
        echo "Created itemsordered table\n<br>";
    }else{
        echo mysqli_error($connection);
        echo "<br>";
    }

    if (mysqli_query($connection, $tablecreatevehicle)){
        echo "Created vehicle table\n<br>";
    }else{
        echo mysqli_error($connection);
        echo "<br>";
    }

    if (mysqli_query($connection, $tablecreatetransport)){
        echo "Created transport table\n<br>";
    }else{
        echo mysqli_error($connection);
        echo "<br>";
    }

    if (mysqli_query($connection, $tablecreatepayment)){
        echo "Created payment table\n<br>";
    }else{
        echo mysqli_error($connection);
        echo "<br>";
    }

    

}    

function deletedb(){
    $server="localhost";
    $username="root";
    $password="root";
    $connection=mysqli_connect($server, $username, $password);

    if(!$connection){
        die("Uh oh, ".mysqli_connect_error()." contact Owais immediately!");
    }

    $dbdelete = "DROP DATABASE foodsystem;";

    if (mysqli_query($connection, $dbdelete)){
        echo "Database deleted successfully\n<br>";

    }else{
        echo mysqli_error($connection);
    }
}

function populatedb(){
    $server="localhost";
    $username="root";
    $password="root";
    $connection=mysqli_connect($server, $username, $password);

    $custid=rand(10000, 60000);
    $orderid=rand(10000000, 900000000);
    $txnid=rand(10000000, 900000000);
    $itemid=rand(10000, 60000);
    $fssaiid=rand(1000000000000, 90000000000000);
    $staffaadhar="123412341234";
    $position="Head Chef";
    $custname="Owais Shaikh";
    $chefname="Chef Mike";
    $custphone="1234567890";
    $staffexperience="12";
    $staffphone="1283932049";
    $deliverymanaadhar="239582942948294";
    $deliverymanname="Peter Parker";
    $deliverymanaddress="Queens";
    $vehiclenumber="KA22C2345";
    $restaurantname="Pizza by Alfredos";
    $restauranttype="Italian";
    $restaurantaddress="Someplace, Somestreet, Somecity, 400096";
    $deliverylocation="Otherplace";
    $deliverystreet="Otherstreet";
    $deliveryarea="Otherarea";
    $deliveryplot="123/A";
    $deliverymanimage="data:image/png;base64,R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7";
    $itemstatus="10";
    $itemname="Cheese pizza";
    $itemprice="200";
    $orderamount="100";
    $itemqty="5";
    $vehicleclass="2WLR";
    $distance="7";
    $route="WEH";
    $totalamount="30";
    $gst="5";
    $paytype="Debit Card";
    $cardholder="OWAIS SHAIKH";
    $cardno="3333948520394893";
    $cardexp="04/22";
    $cvv="300";


    if(!$connection){
        die("Uh oh, ".mysqli_connect_error()." contact Owais immediately!");
    }

    $dbuse="USE foodsystem;";

    if (mysqli_query($connection, $dbuse)){
        echo "Selected database\n<br>";
    }else{
        echo "Error selecting database: <br>" . mysqli_error($connection);
    }    

    $tableinsertcustomer="INSERT INTO customer values(
        '$custname',
        '$custid',
        '$custphone'
    );";

    $tableinsertstaff="INSERT INTO staff values(
        '$staffaadhar',
        '$chefname',
        '$position',
        '$staffexperience'
    );";

    $tableinsertrestaurant="INSERT INTO restaurant values(
        '$fssaiid',
        '$restaurantname',
        '$staffphone',
        '$restauranttype',
        '$restaurantaddress',
        '$staffaadhar'
    );";
    $tableinsertlocation="INSERT INTO location values(
        '$custid',
        '$deliverylocation',
        '$deliverystreet',
        '$deliveryarea',
        '$deliveryplot'
    );";
    $tableinsertdeliveryman="INSERT INTO deliveryman values(
        '$deliverymanaadhar',
        '$deliverymanname',
        '$deliverymanname',
        '$deliverymanimage'
    );";
    $tableinsertitem="INSERT INTO item values(
        '$itemid',
        '$itemname',
        '$itemprice',
        '$itemstatus'
    );";
    $tableinsertorder="INSERT INTO orders values(
        '$orderid',
        NOW(),
        '$orderamount'
    );";
    $tableinsertitemsordered="INSERT INTO items_ordered values(
        '$custid',
        '$orderid',
        '$itemid',
        '$itemqty'
    );";
    $tableinsertvehicle="INSERT INTO vehicle values(
        '$vehicleclass',
        '$vehiclenumber'
    );";
    $tableinserttransport="INSERT INTO transport values(
        '$custid',
        '$deliverymanaadhar',
        '$distance',
        '$route',
        '$vehiclenumber'
    );";
    $tableinsertpayment="INSERT INTO payment values(
        '$txnid',
        '$custid',
        '$totalamount',
        '$gst',
        '$paytype',
        '$cardholder',
        '$cardno',
        '$cardexp',
        '$cvv'
    );";

    if (mysqli_query($connection, $tableinsertcustomer)){
        echo "Inserted in customer table\n<br>";
    }else{
        echo mysqli_error($connection);
        echo "<br>";
    }    

    if (mysqli_query($connection, $tableinsertlocation)){
        echo "Inserted in location table\n<br>";
    }else{
        echo mysqli_error($connection);
        echo "<br>";
    }

    if (mysqli_query($connection, $tableinsertstaff)){
        echo "Inserted in staff table\n<br>";
    }else{
        echo mysqli_error($connection);
        echo "<br>";
    }

    if (mysqli_query($connection, $tableinsertrestaurant)){
        echo "Inserted in restaurant table\n<br>";
    }else{
        echo mysqli_error($connection);
        echo "<br>";
    }

    if (mysqli_query($connection, $tableinsertdeliveryman)){
        echo "Inserted in deliveryman table\n<br>";
    }else{
        echo mysqli_error($connection);
        echo "<br>";
    }

    if (mysqli_query($connection, $tableinsertitem)){
        echo "Inserted in item table\n<br>";
    }else{
        echo mysqli_error($connection);
        echo "<br>";
    }

    if (mysqli_query($connection, $tableinsertorder)){
        echo "Inserted in order table\n<br>";
    }else{
        echo mysqli_error($connection);
        echo "<br>";
    }

    if (mysqli_query($connection, $tableinsertitemsordered)){
        echo "Inserted in itemsordered table\n<br>";
    }else{
        echo mysqli_error($connection);
        echo "<br>";
    }

    if (mysqli_query($connection, $tableinsertvehicle)){
        echo "Inserted in vehicle table\n<br>";
    }else{
        echo mysqli_error($connection);
        echo "<br>";
    }

    if (mysqli_query($connection, $tableinserttransport)){
        echo "Inserted in transport table\n<br>";
    }else{
        echo mysqli_error($connection);
        echo "<br>";
    }

    if (mysqli_query($connection, $tableinsertpayment)){
        echo "Inserted in payment table\n<br>";
    }else{
        echo mysqli_error($connection);
        echo "<br>";
    }
}


?>




